package com.example.pairing;

public class ParticipantsDBFields {
  public final int PARTICIPANT_ID_INDEX = 0;
  public final int NAME_INDEX = 1;
  public final int ACTIVE_INDEX = 2;
}
