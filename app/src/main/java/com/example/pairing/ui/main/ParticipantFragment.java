package com.example.pairing.ui.main;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.*;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.example.pairing.PairsStorage;
import com.example.pairing.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import java.util.LinkedList;
import java.util.List;

class User {
  String name;
  String userID;
  String status;
}


/**
 * A placeholder fragment containing a simple view.
 */
public class ParticipantFragment extends Fragment {

  private static final String ARG_SECTION_NUMBER = "section_number";

  private PageViewModel pageViewModel;

  PairsStorage pairsStorage;
  String userName;
  String userId;
  String userStatus;
  List<User> userList = new LinkedList<>();

  private int iSelectedRowIdx;

  View root;
  private EditText newUserNameET;
  private String newUserName;


  public static ParticipantFragment newInstance(int index) {
    ParticipantFragment fragment = new ParticipantFragment();
    Bundle bundle = new Bundle();
    bundle.putInt(ARG_SECTION_NUMBER, index);
    fragment.setArguments(bundle);
    return fragment;
  }

  void createNewRow(final TableLayout tl, final TableLayout.LayoutParams lparams, String name, String active) {

    TableRow row = new TableRow(getContext());
    row.setClickable(true);  //allows you to select a specific row
//    Drawable shadowDrawable = getResources().getDrawable(android.R.drawable.dialog_holo_light_frame);
//      row.setBackground(shadowDrawable);
    lparams.setMargins(10, 10, 10, 10);
    //row.setPadding(0,0,0,20);
    row.setLayoutParams(lparams);

    SwitchCompat onOffToggleSwitch = (SwitchCompat) getLayoutInflater().inflate(R.layout.switch_template, null);
    TableRow.LayoutParams switchParams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    switchParams.weight = 1;
    //switchParams.topMargin = 20;
    onOffToggleSwitch.setBackgroundColor(Color.WHITE);
    if (active.equals("active"))
      onOffToggleSwitch.setChecked(true);
    else
      onOffToggleSwitch.setChecked(false);

    onOffToggleSwitch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        //get the index of the row in the TableLayout
        ViewGroup rowView = (ViewGroup) v.getParent();
        int idx = tl.indexOfChild(rowView);

        //use the row index to reference the associated object in the LinkedList.
        //we need to get the linkedlist element b/c it contains the request ID
        //for the PendingIntent
        User participant = userList.get(idx);

        //notifyBtn object will be toggled on or off and different color will be applied accordingly
        SwitchCompat onOffSwitch = (SwitchCompat) v;
        if (participant.status.equals("not active")) {
          onOffSwitch.setChecked(true);
          participant.status = "active";
        }
        else {
          onOffSwitch.setChecked(false);
          participant.status = "not active";
        }
        //update local memory
        userList.set(idx, participant);

        //update dataabase
        PairsStorage pairsStorage = PairsStorage.getInstance(getContext());
        pairsStorage.updateParticipantDBfield(PairsStorage.ACTIVE, participant.status, participant.name);
        //pairsStorage.close();

        TableLayout tl = (TableLayout) root.findViewById(R.id.tableLayout);
        tl.removeAllViews();
        userList.clear();
        showUsers();
      }
    });

    View deleteBtnTemplate = (View) getLayoutInflater().inflate(R.layout.delete_btn, null);
    deleteBtnTemplate.setVisibility(View.VISIBLE);
    TableRow.LayoutParams deleteLayoutParams =
      new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT);
    deleteBtnTemplate.setLayoutParams(deleteLayoutParams);

    View deleteBtnView = deleteBtnTemplate.findViewById(R.id.imageButton);

    deleteBtnView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        PairsStorage pairsStorage = PairsStorage.getInstance(getContext());
        //pairsStorage.deleteParticipantById();
        User user = userList.get(iSelectedRowIdx);
        pairsStorage.deleteParticipantByName(user.name);
        userList.remove(iSelectedRowIdx);
        //remove the associated row item from the GUI
        TableLayout tl = (TableLayout) root.findViewById(R.id.tableLayout);
        tl.removeViewAt(iSelectedRowIdx);
      }
    });


    row.setOnLongClickListener(new View.OnLongClickListener() {

      @Override
      public boolean onLongClick(View v) {
        Log.d("event", "long click");
        //get the index of the row in the TableLayout
        iSelectedRowIdx = tl.indexOfChild(v);

        TableRow tablerow = (TableRow) v;
        TextView view = (TextView) tablerow.getChildAt(0);
        view.setBackgroundColor(getResources().getColor(R.color.colorTouch));

        View deleteView = (View) tablerow.getChildAt(2);
        deleteView.setVisibility(View.VISIBLE);

        return true;
      }
    });

    TextView participantNameTV = new TextView(getContext());
    participantNameTV.setText(name);
    TableRow.LayoutParams timeTVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    timeTVparams.weight = 6;
    participantNameTV.setLayoutParams(timeTVparams);
    participantNameTV.setPadding(10, 2, 10, 2);
    participantNameTV.setBackgroundColor(Color.WHITE);

    row.setPadding(5, 5, 5, 5);
    row.addView(participantNameTV);
    row.addView(onOffToggleSwitch);
    row.addView(deleteBtnTemplate);
    tl.addView(row, 0);
  }

  void showUsers() {
    pairsStorage = PairsStorage.getInstance(getContext());
    Cursor res = pairsStorage.getAllParticipants();
    if (res.getCount() == 0) {
    }
    else {

      TableLayout tl = null;
      TableLayout.LayoutParams lparams = null;
      tl = (TableLayout) root.findViewById(R.id.tableLayout);
      lparams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

      while (res.moveToNext()) {
        //get the data
        userName = res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
        userStatus = res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.ACTIVE_INDEX);
        userId = "" + res.getInt(PairsStorage.PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);

        createNewRow(tl, lparams, userName, userStatus);
        User user = new User();
        user.userID = userId;
        user.name = userName;
        user.status = userStatus;
        userList.add(0, user);
      }
    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
    int index = 1;
    if (getArguments() != null) {
      index = getArguments().getInt(ARG_SECTION_NUMBER);
    }
    pageViewModel.setIndex(index);
  }

  AlertDialog dlg;
  AlertDialog.Builder builder;
  View addParticipantView;

  private void createAlertDlg() {
    builder = new AlertDialog.Builder(getContext());
    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        newUserName = newUserNameET.getText().toString();
        //save to participant table in DB
        pairsStorage = PairsStorage.getInstance(getContext());
        pairsStorage.addParticipant(newUserName, "active");
        //pairsStorage.close();
      }
    });

    builder.setNegativeButton("DISMISS", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

      }
    });
    TextView title = new TextView(getContext());
    title.setText("Add new user");
    title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    title.setPadding(20, 20, 20, 20);
    title.setGravity(Gravity.CENTER);
    title.setTextColor(Color.WHITE);
    title.setTextSize(20);
    addParticipantView = getLayoutInflater().inflate(R.layout.add_participant_dlg, null);
    builder.setView(addParticipantView);
    builder.setCustomTitle(title);
    dlg = builder.create();
    dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    dlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialog) {
        TableLayout tl = (TableLayout) root.findViewById(R.id.tableLayout);
        tl.removeAllViews();
        userList.clear();
        showUsers();
      }
    });

  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    root = inflater.inflate(R.layout.participant_fragment_main, container, false);
    FloatingActionButton fab = root.findViewById(R.id.fab);

    createAlertDlg();

    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        newUserNameET = (EditText) addParticipantView.findViewById(R.id.participant_edittext);
        dlg.show();
      }
    });


    showUsers();

    return root;
  }
}
