package com.example.pairing.ui.main;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.example.pairing.R;

public class GeneratePairDlg extends DialogFragment {

  View shuffleBtn;
  View acceptBtn;
  View generatePairsView;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  static GeneratePairDlg newInstance(int num) {
    GeneratePairDlg f = new GeneratePairDlg();

    // Supply num input as an argument.
    Bundle args = new Bundle();
    args.putInt("num", num);
    f.setArguments(args);

    return f;
  }

  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.participant_fragment_main, container, false);

    return view;
  }


  @NonNull
  @Override
  public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    Dialog dialog = super.onCreateDialog(savedInstanceState);

    generatePairsView = getLayoutInflater().inflate(R.layout.generate_pairs_dlg, null);

    dialog.setContentView(generatePairsView);

    return dialog;
  }
}
