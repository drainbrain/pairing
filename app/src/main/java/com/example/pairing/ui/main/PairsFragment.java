package com.example.pairing.ui.main;

import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.*;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import com.example.pairing.PairsStorage;
import com.example.pairing.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.*;
import java.util.stream.Collectors;

class Pair {
  Pair(){
    skippedCount=0;
    timesPairedCount=0;
  }
  String pairId;
  String name1;
  String name2;
  int skippedCount;
  int timesPairedCount;
}

class Participant{
  String name;
  String id;
  boolean paired;
}

public class PairsFragment extends Fragment {
  private static final String ARG_SECTION_NUMBER = "section_number";
  private PageViewModel pageViewModel;

  PairsStorage pairsStorage;
  String participant1;
  String participant2;
  String pairId;
  List<Pair> allExistingPairsFromDB = new LinkedList<>();
  HashMap<String, String> newPairsHashmap = new HashMap<>();
  String pairs = "";

  View root;
  private int iSelectedRowIdx;

  public static PairsFragment newInstance(int index) {
    PairsFragment fragment = new PairsFragment();
    Bundle bundle = new Bundle();
    bundle.putInt(ARG_SECTION_NUMBER, index);
    fragment.setArguments(bundle);
    return fragment;
  }

  void createNewRow(final TableLayout tl,
                    final TableLayout.LayoutParams lparams,
                    String participant1,
                    String participant2,
                    String numTimesPaired,
                    String skippedCount)
  {
    int LEFT_MARGIN_SIZE = 0;
    int RIGHT_MARGIN_SIZE = 0;

    TableRow row = new TableRow(getContext());
    row.setClickable(true);  //allows you to select a specific row
    lparams.setMargins(10, 10, 10, 10);
    row.setLayoutParams(lparams);

    TextView participant1TV = new TextView(getContext());
    participant1TV.setText(participant1);
    TableRow.LayoutParams participant1TVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    participant1TVparams.weight = 6;
    participant1TV.setLayoutParams(participant1TVparams);
    participant1TV.setPadding(LEFT_MARGIN_SIZE, 2, RIGHT_MARGIN_SIZE, 2);
    participant1TV.setBackgroundColor(Color.WHITE);

    TextView participant2TV = new TextView(getContext());
    participant2TV.setText(participant2);
    TableRow.LayoutParams participant2TVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    participant2TVparams.weight = 6;
    participant2TV.setLayoutParams(participant2TVparams);
    participant2TV.setPadding(LEFT_MARGIN_SIZE, 2, RIGHT_MARGIN_SIZE, 2);
    participant2TV.setBackgroundColor(Color.WHITE);

    TextView numTimesPairedTV = new TextView(getContext());
    numTimesPairedTV.setText(numTimesPaired);
    TableRow.LayoutParams numTimesPairedTVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    numTimesPairedTVparams.weight = 6;
    numTimesPairedTV.setLayoutParams(numTimesPairedTVparams);
    numTimesPairedTV.setPadding(LEFT_MARGIN_SIZE, 2, RIGHT_MARGIN_SIZE, 2);
    numTimesPairedTV.setBackgroundColor(Color.WHITE);

    TextView numSkippedTV = new TextView(getContext());
    numSkippedTV.setText(skippedCount);
    TableRow.LayoutParams numSkippedTVparams = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT);
    numSkippedTVparams.weight = 6;
    numSkippedTV.setLayoutParams(numSkippedTVparams);
    numSkippedTV.setPadding(LEFT_MARGIN_SIZE, 2, RIGHT_MARGIN_SIZE, 2);
    numSkippedTV.setBackgroundColor(Color.WHITE);

    View editBtnTemplate = (View) getLayoutInflater().inflate(R.layout.edit_btn, null);
    editBtnTemplate.setPadding(0, 2, RIGHT_MARGIN_SIZE, 2);
    editBtnTemplate.setVisibility(View.VISIBLE);
    TableRow.LayoutParams editLayoutParams =
      new TableRow.LayoutParams(
        TableRow.LayoutParams.MATCH_PARENT,
        TableRow.LayoutParams.MATCH_PARENT);
    editBtnTemplate.setLayoutParams(editLayoutParams);

    View editBtnView = editBtnTemplate.findViewById(R.id.imageButton);

    editBtnView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        PairsStorage pairsStorage = PairsStorage.getInstance(getContext());
        //pairsStorage.deleteParticipantById();
        Pair pair = allExistingPairsFromDB.get(tl.indexOfChild((View) v.getParent()));

        //bring up dialog with pairs data
        createEditPairDlg(pair);
        editPairDlg.show();
      }
    });

    row.setPadding(5, 5, 5, 5);
    row.addView(participant1TV);
    row.addView(participant2TV);
    row.addView(numTimesPairedTV);
    row.addView(numSkippedTV);
    row.addView(editBtnTemplate);
    tl.addView(row, 0);
  }

  List<Pair> getAllPairsForParticipantById(String participantId){
    pairsStorage = PairsStorage.getInstance(getContext());
    Cursor pairsRes = pairsStorage.getAllPairsById(Integer.parseInt(participantId));
    if (pairsRes.getCount() == 0) {
    }
    else {

      TableLayout tableLayout = null;
      TableLayout.LayoutParams lparams = null;
      tableLayout = (TableLayout) root.findViewById(R.id.tableLayout);
      lparams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

      System.out.println("Existing pairs list");
      System.out.println("===================");

      List<Pair> existingPairsFromDBForParticipant = new ArrayList<>();

      while (pairsRes.moveToNext()) {
        //get the data
        Integer participant1Id = pairsRes.getInt(PairsStorage.PAIR_FIELD_INDEXES.PARTICIPANT1_ID_INDEX);
        Integer participant2Id = pairsRes.getInt(PairsStorage.PAIR_FIELD_INDEXES.PARTICIPANT2_ID_INDEX);
        String numTimesPaired = pairsRes.getString(PairsStorage.PAIR_FIELD_INDEXES.TIMES_PAIRED_INDEX);
        String consecutiveTimeSkippedCount = pairsRes.getString(PairsStorage.PAIR_FIELD_INDEXES.CONSECUTIVE_TIMES_SKIPPED_COUNT);

        Cursor participant1res = pairsStorage.getParticipantById(participant1Id);
        if(participant1res.moveToNext()) {
          String localParticipant1 = participant1res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
          String participant1Status = participant1res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.ACTIVE_INDEX);
          if(participant1Status.equals("active")) {
            Cursor participant2res = pairsStorage.getParticipantById(participant2Id);
            if (participant2res.moveToNext()) {
              String localParticipant2 = participant2res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
              String participant2Status = participant2res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.ACTIVE_INDEX);
              if(participant2Status.equals("active")) {
                createNewRow(tableLayout, lparams, localParticipant1, localParticipant2, numTimesPaired, consecutiveTimeSkippedCount);
                Pair pair = new Pair();
                pair.name1 = localParticipant1;
                pair.name2 = localParticipant2;
                pair.skippedCount = Integer.parseInt(consecutiveTimeSkippedCount);
                System.out.println("pair: " + participant1Id + ", " + participant2Id + ", " + localParticipant1 + " " + localParticipant2);
                existingPairsFromDBForParticipant.add(pair);
              }
            }
          }
        }
      }
      return existingPairsFromDBForParticipant;
    }
    return null;
  }

  void showPairs() {
    allExistingPairsFromDB.clear();
    pairsStorage = PairsStorage.getInstance(getContext());
    Cursor pairsRes = pairsStorage.getAllPairs();
    if (pairsRes.getCount() == 0) {
    }
    else {

      TableLayout tableLayout = null;
      TableLayout.LayoutParams lparams = null;
      tableLayout = (TableLayout) root.findViewById(R.id.tableLayout);
      lparams = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT);

      System.out.println("Existing pairs list");
      System.out.println("===================");

      while (pairsRes.moveToNext()) {
        //get the data
        Integer participant1Id = pairsRes.getInt(PairsStorage.PAIR_FIELD_INDEXES.PARTICIPANT1_ID_INDEX);
        Integer participant2Id = pairsRes.getInt(PairsStorage.PAIR_FIELD_INDEXES.PARTICIPANT2_ID_INDEX);
        String numTimesPaired = pairsRes.getString(PairsStorage.PAIR_FIELD_INDEXES.TIMES_PAIRED_INDEX);
        String consecutiveTimesSkipped = pairsRes.getString(PairsStorage.PAIR_FIELD_INDEXES.CONSECUTIVE_TIMES_SKIPPED_COUNT);
        pairId = "" + pairsRes.getInt(PairsStorage.PAIR_FIELD_INDEXES.PAIR_ID_INDEX);

        Cursor participant1res = pairsStorage.getParticipantById(participant1Id);
        if(participant1res.moveToNext()) {
          participant1 = participant1res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);

          Cursor participant2res = pairsStorage.getParticipantById(participant2Id);
          if(participant2res.moveToNext()) {
            participant2 = participant2res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
            createNewRow(tableLayout, lparams, participant1, participant2, numTimesPaired, consecutiveTimesSkipped);
            Pair pair = new Pair();
            pair.name1 = participant1;
            pair.name2 = participant2;
            pair.pairId = pairId;
            pair.timesPairedCount = Integer.parseInt(numTimesPaired);
            pair.skippedCount = Integer.parseInt(consecutiveTimesSkipped);
            System.out.println("pair: " + participant1Id + ", " + participant2Id + ", " + participant1 + " " + participant2);
            allExistingPairsFromDB.add(0, pair);
          }
        }
      }
    }
  }

  List<Participant> participantsList = new ArrayList<>();
  HashMap<String, Participant> participantHashmap = new HashMap<>();

  void getParticipants() {
    pairsStorage = PairsStorage.getInstance(getContext());
    Cursor res = pairsStorage.getAllParticipants();
    if (res.getCount() == 0) {
    }
    else {
      while (res.moveToNext()) {
        String userStatus = res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.ACTIVE_INDEX);
        if(userStatus.equals("active")) {
          Participant participant = new Participant();
          participant.name = res.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
          participant.id = "" + res.getInt(PairsStorage.PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
          participant.paired = false;
          participantsList.add(participant);
          participantHashmap.put(participant.id, participant);
        }
      }
    }
  }

  @RequiresApi(api = Build.VERSION_CODES.N)
  List<Pair> cleanupPairsList(List<Pair> pairsFromDB) {
    List<Pair> cleanedUpList = new ArrayList<>();


    for(Pair pair: pairsFromDB){
      Optional<Participant> participant1 = participantsList.stream().filter(participant -> {
        return pair.name1.equals(participant.name);
      }).findFirst();
      Optional<Participant> participant2 = participantsList.stream().filter(participant -> {
        return pair.name2.equals(participant.name);
      }).findFirst();

      if((participant1.isPresent() && !participant1.get().paired) &&
        ((participant2.isPresent() && !participant2.get().paired)))
      {
        Pair cleanPair = new Pair();
        cleanPair.name1 = pair.name1;
        cleanPair.name2 = pair.name2;
        cleanPair.skippedCount = pair.skippedCount;
        cleanPair.pairId = pair.pairId;
        cleanPair.timesPairedCount = pair.timesPairedCount;
        cleanedUpList.add(cleanPair);
      }
    }

    return cleanedUpList;
  }

  private int numPairsToChooseFrom(List<Pair> pairsListFromDB){
    int previousSkippedCount = 0;
    int numPairsToConsider = 0;
    //get the number of possible new pairs for the participant
    for(int pairIdx = 0; pairIdx < pairsListFromDB.size(); pairIdx++) {
      if(pairsListFromDB.get(pairIdx).skippedCount < previousSkippedCount){
        break;
      }
      previousSkippedCount = pairsListFromDB.get(pairIdx).skippedCount;
      ++numPairsToConsider;
    }

    return numPairsToConsider;
  }

  private Pair createNewPair(int numPairsToConsider, List<Pair> usablePairsListFromDB, List<Pair> existingPairsFromDBForParticipant){
    if(numPairsToConsider == 1) { //if there is only one then that is the only new pair possible
      if(participantsList.size() % 2 == 0) {
        return usablePairsListFromDB.get(0);
      }
      else{
        return existingPairsFromDBForParticipant.get(0);
      }
    }
    else{ //otherwise flip a coin among the possible pairs
      int pickedPairIdx = new Random().nextInt(numPairsToConsider);
      if(participantsList.size() % 2 == 0) {
        return usablePairsListFromDB.get(pickedPairIdx);
      }
      else{
        return existingPairsFromDBForParticipant.get(pickedPairIdx);
      }
    }
  }


  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    pageViewModel = ViewModelProviders.of(this).get(PageViewModel.class);
    int index = 1;
    if (getArguments() != null) {
      index = getArguments().getInt(ARG_SECTION_NUMBER);
    }
    pageViewModel.setIndex(index);
  }

  AlertDialog editPairDlg;
  AlertDialog.Builder editPairBuilder;
  View editPairView;

  AlertDialog generateNewPairsDlg;
  AlertDialog.Builder generateNewPairsBuilder;
  View generatePairsView;

  ArrayList<String> leftList = new ArrayList<>();
  ArrayList<String> rightList = new ArrayList<>();

  private void createGenerateNewPairsAlertDlg(){
    generateNewPairsBuilder = new AlertDialog.Builder(getContext());
    generateNewPairsBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if(pairs != null) {
          //save to pairs table in DB
          pairsStorage = PairsStorage.getInstance(getContext());

          //parse pairs string
          String[] pairsTokens = pairs.split("\n");

          for (int i = 0; i < pairsTokens.length; i++) {
            //save pair to db
            //pairsStorage.addPair(newUserName, "active");
          }
        }
      }
    });

    generateNewPairsBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {
        newPairsHashmap.clear();
      }
    });


    generateNewPairsBuilder.setNeutralButton("Generate Pairs", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
      }
    });

    TextView title = new TextView(getContext());
    title.setText("Generate Pairs");
    title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    title.setPadding(20, 20, 20, 20);
    title.setGravity(Gravity.CENTER);
    title.setTextColor(Color.WHITE);
    title.setTextSize(20);
    generatePairsView = getLayoutInflater().inflate(R.layout.generate_pairs_dlg, null);
    generateNewPairsBuilder.setView(generatePairsView);
    generateNewPairsBuilder.setCustomTitle(title);
    generateNewPairsDlg = generateNewPairsBuilder.create();
    generateNewPairsDlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    generateNewPairsDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @RequiresApi(api = Build.VERSION_CODES.N)
      @Override
      public void onDismiss(DialogInterface dialog) {
        TableLayout tableLayout = (TableLayout) root.findViewById(R.id.tableLayout);
        tableLayout.removeAllViews();
        //save all pair updates to the database
        pairsStorage = PairsStorage.getInstance(getContext());
        //pairsStorage.deleteAllPairs();
        if(newPairsHashmap.size() > 0) {
          for (int i = 0; i < allExistingPairsFromDB.size(); i++) {
            String name1 = allExistingPairsFromDB.get(i).name1;
            String name2 = allExistingPairsFromDB.get(i).name2;
            //update pair
            if (newPairsHashmap.get(name1 + name2) == null && newPairsHashmap.get(name2 + name1) == null) {
              pairsStorage.updatePair(name1, name2, false);
            }
            else {
              pairsStorage.updatePair(name1, name2, true);
            }
          }
        }
        leftList.clear();
        rightList.clear();
        newPairsHashmap.clear();
        participantsList.stream().forEach(participant -> participant.paired = false);
        showPairs();
      }
    });

    generateNewPairsDlg.setOnShowListener(new DialogInterface.OnShowListener() {
      @Override
      public void onShow(DialogInterface dialog) {
        Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEUTRAL);
        button.setOnClickListener(new View.OnClickListener() {
          @RequiresApi(api = Build.VERSION_CODES.N)
          @Override
          public void onClick(View view) {
            //populate local participant list
            pairs = "";
            participantsList.clear();
            leftList.clear();
            rightList.clear();
            getParticipants();
            int numParticipants = participantsList.size();
            //temp variable used in main loop
            String participantName1;
            String participantName2;
            //generate pairs from local participant list sorted by skipped count descending
            for(int currentParticipantIdx = 0; currentParticipantIdx < participantsList.size(); currentParticipantIdx++) {
              if(participantsList.get(currentParticipantIdx).paired)
                continue;
              //get all pairs with participant
              List<Pair> existingPairsFromDBForParticipant = getAllPairsForParticipantById(participantsList.get(currentParticipantIdx).id);
              //clean the list of pairs that are unusable due to one of the participants already having been newly paired
              List<Pair> usablePairsListFromDB = cleanupPairsList(existingPairsFromDBForParticipant);

              //get the number of possible new pairs for the participant
              int numPairsToConsider = 0;
              if((numPairsToConsider = numPairsToChooseFrom(usablePairsListFromDB)) == 0)
                if((numPairsToConsider = numPairsToChooseFrom(existingPairsFromDBForParticipant)) == 0)
                  break;

              //generate the new pair
              Pair generatedPair = createNewPair(numPairsToConsider, usablePairsListFromDB, existingPairsFromDBForParticipant);
              participantName1 = generatedPair.name1;
              participantName2 = generatedPair.name2;

              System.out.println("new pair: " + participantName1 + ", " + participantName2 + ",  num pairs considered = " + numPairsToConsider);

              final String pn1 = participantName1;
              final String pn2 = participantName2;
              newPairsHashmap.put(participantName1+participantName2, participantName2+participantName2);
              participantsList.stream().filter(participant -> pn1.equals(participant.name)).collect(Collectors.toList()).get(0).paired = true;
              participantsList.stream().filter(participant -> pn2.equals(participant.name)).collect(Collectors.toList()).get(0).paired = true;
              leftList.add(participantName1);
              rightList.add(participantName2);
            }

            ArrayAdapter leftAdapter = new ArrayAdapter<String>(getContext(), R.layout.participant_text_view, leftList);
            ArrayAdapter rightAdapter = new ArrayAdapter<String>(getContext(), R.layout.participant_text_view, rightList);

            ListView leftLV = (ListView) generatePairsView.findViewById(R.id.left_participant);
            leftLV.setAdapter(leftAdapter);
            ListView rightLV = (ListView) generatePairsView.findViewById(R.id.right_participant);
            rightLV.setAdapter(rightAdapter);
          }
        });
      }
    });

  }

  private void createEditPairDlg(Pair pair){
    editPairBuilder = new AlertDialog.Builder(getContext());
    editPairBuilder.setPositiveButton("Update Pair", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if(pairs != null) {
          //save to pairs table in DB
          pairsStorage = PairsStorage.getInstance(getContext());

          //parse pairs string
          String[] pairsTokens = pairs.split("\n");

          for (int i = 0; i < pairsTokens.length; i++) {
            //save pair to db
            //pairsStorage.addPair(newUserName, "active");
          }
        }
      }
    });

    editPairBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialogInterface, int i) {

      }
    });

    TextView title = new TextView(getContext());
    title.setText("Update Pair");
    title.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    title.setPadding(20, 20, 20, 20);
    title.setGravity(Gravity.CENTER);
    title.setTextColor(Color.WHITE);
    title.setTextSize(20);
    editPairView = getLayoutInflater().inflate(R.layout.edit_pair_dlg, null);
    EditText leftParticipant = editPairView.findViewById(R.id.left_participant);
    leftParticipant.setText(pair.name1);
    EditText rightParticipant = editPairView.findViewById(R.id.right_participant);
    rightParticipant.setText(pair.name2);
    EditText timesPaired = editPairView.findViewById(R.id.times_paired);
    timesPaired.setText("" + pair.timesPairedCount);
    EditText timesSkipped = editPairView.findViewById(R.id.times_skipped);
    timesSkipped.setText("" + pair.skippedCount);
    editPairBuilder.setView(editPairView);
    editPairBuilder.setCustomTitle(title);
    editPairDlg = editPairBuilder.create();
    editPairDlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
    editPairDlg.setOnDismissListener(new DialogInterface.OnDismissListener() {
      @Override
      public void onDismiss(DialogInterface dialog) {
        TableLayout tableLayout = (TableLayout) root.findViewById(R.id.tableLayout);
        tableLayout.removeAllViews();
        //save all pair updates to the database
        pairsStorage = PairsStorage.getInstance(getContext());
        pairsStorage.updatePair(pair.name1, pair.name2,
          Integer.parseInt(timesPaired.getText().toString()),
          Integer.parseInt(timesSkipped.getText().toString()));

        showPairs();
      }
    });
  }


  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

    pairsStorage = PairsStorage.getInstance(getContext());

    root = inflater.inflate(R.layout.pairs_fragment_main, container, false);
    FloatingActionButton fab_shuffle = root.findViewById(R.id.fab_shuffle);

    createGenerateNewPairsAlertDlg();

    fab_shuffle.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        generateNewPairsDlg.show();
      }
    });

    showPairs();

    FloatingActionButton fab_init = root.findViewById(R.id.fab_init);
    fab_init.setOnClickListener(new View.OnClickListener() {
      @RequiresApi(api = Build.VERSION_CODES.N)
      @Override
      public void onClick(View v) {
        pairsStorage = PairsStorage.getInstance(getContext());
        pairsStorage.initPairs();
/*
        allExistingPairsFromDB.stream().forEach(pair -> {
          pair.skippedCount = 0;
          pair.timesPairedCount = 0;
        });
*/

        newPairsHashmap.clear();
        participantsList.stream().forEach(participant -> participant.paired = false);

        showPairs();
      }
    });


    FloatingActionButton fab_add = root.findViewById(R.id.fab_delete);
    fab_add.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        pairsStorage = PairsStorage.getInstance(getContext());
        pairsStorage.deleteAllPairs();
        TableLayout tableLayout = (TableLayout) root.findViewById(R.id.tableLayout);
        tableLayout.removeAllViews();
        leftList.clear();
        rightList.clear();
      }
    });

    return root;
  }

}
