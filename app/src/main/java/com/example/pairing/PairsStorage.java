package com.example.pairing;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;


public class PairsStorage extends SQLiteOpenHelper {

  public static final String DATABASE_NAME = "Pairs.db";

  public static final String PARTICIPANT_TABLE = "participants";
  public static final String PARTICIPANT_ID = "id";
  public static final String NAME = "name";
  public static final String ACTIVE = "active";

  public static final String PAIRS_TABLE = "pairs";
  public static final String PAIR_ID = "id";
  public static final String PARTICIPANT1_ID = "participant1_id";
  public static final String PARTICIPANT2_ID = "participant2_id";
  public static final String LAST_PAIRED_DATE = "last_paired";
  public static final String TIMES_PAIRED = "times_paired";
  public static final String CONSECUTIVE_TIMES_SKIPPED = "times_skipped";


  public static final ParticipantsDBFields PARTICIPANT_FIELD_INDEXES = new ParticipantsDBFields();
  public static final PairsDBFields PAIR_FIELD_INDEXES = new PairsDBFields();

  private static PairsStorage sInstance;


  public static synchronized PairsStorage getInstance(Context context) {

    // Use the application context, which will ensure that you
    // don't accidentally leak an Activity's context.
    // See this article for more information: http://bit.ly/6LRzfx
    if (sInstance == null) {
      sInstance = new PairsStorage(context.getApplicationContext());
    }
    return sInstance;
  }

  private PairsStorage(Context context) {
    super(context, DATABASE_NAME, null, 1);
  }

  @Override
  public void onCreate(SQLiteDatabase db) {
    db.execSQL("CREATE TABLE " + PARTICIPANT_TABLE +
      " (" +
      PARTICIPANT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
      NAME + " TEXT," +
      ACTIVE + " TEXT," +
      " UNIQUE(" + NAME + ")" +
      ")");

    db.execSQL("CREATE TABLE " + PAIRS_TABLE +
      " (" + PAIR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
      PARTICIPANT1_ID + " INTEGER NOT NULL," +
      PARTICIPANT2_ID + " INTEGER NOT NULL," +
      LAST_PAIRED_DATE + " TEXT," +
      TIMES_PAIRED + " INTEGER," +
      CONSECUTIVE_TIMES_SKIPPED + " INTEGER, " +
      " UNIQUE(" + PARTICIPANT1_ID + "," + PARTICIPANT2_ID + ")," +
      " FOREIGN KEY (" + PARTICIPANT1_ID + ") REFERENCES id (" + PARTICIPANT_TABLE + ")," +
      " FOREIGN KEY (" + PARTICIPANT2_ID + ") REFERENCES id (" + PARTICIPANT_TABLE + ")" +
      ")");
  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    db.execSQL("DROP TABLE IF EXISTS " + PARTICIPANT_TABLE);
    onCreate(db);
  }

  public Cursor getAllParticipants() {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("SELECT * FROM " + PARTICIPANT_TABLE +
      " ORDER BY " + ACTIVE + " DESC", null);
    return res;
  }

  public Cursor getParticipantByName(String name) {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("SELECT * FROM " + PARTICIPANT_TABLE + " where " + NAME + " = \'" + name + "\'", null);
    return res;
  }

  public Cursor getParticipantById(Integer participantId) {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("select * from " + PARTICIPANT_TABLE + " where " + PARTICIPANT_ID + " = " + participantId, null);
    return res;
  }

  public boolean addParticipant(String name, String strActive) {
    boolean isInserted = insertParticipant(name, strActive);
    if (isInserted == true) {
      System.out.println("Participant Inserted");
      //add new pairs
      Cursor allParticipantsCursor = getAllParticipants();
      while (allParticipantsCursor.moveToNext()) {
        String userName = allParticipantsCursor.getString(PairsStorage.PARTICIPANT_FIELD_INDEXES.NAME_INDEX);
        if (!userName.equals(name))
          addPair(name, userName);
      }
    }
    else
      System.out.println("Participant Not Inserted");

    return isInserted;
  }

  public void updateParticipantDBfield(String strDBfield, String val, String strReqCode) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues cv = new ContentValues();
    cv.put(strDBfield, val); //These Fields should be your String values of actual column names
    db.update(PairsStorage.PARTICIPANT_TABLE, cv, " name = ?", new String[]{strReqCode});
  }

  public boolean insertParticipant(String name, String strActive) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues contentValues = new ContentValues();
    contentValues.put(NAME, name);
    contentValues.put(ACTIVE, strActive);
    try {
      db.insertOrThrow(PARTICIPANT_TABLE, null, contentValues);
      return true;
    }
    catch (Exception e) {
      String ex = e.getMessage();
      Log.d("exception", ex);
      return false;
    }
  }

  public Integer deleteParticipantByName(String name) {
    SQLiteDatabase db = getWritableDatabase();
    return db.delete(PARTICIPANT_TABLE, NAME + " = ?", new String[]{name});
  }

  public Integer deleteParticipantById(String id) {
    SQLiteDatabase db = getWritableDatabase();
    return db.delete(PARTICIPANT_TABLE, PARTICIPANT_ID + " = ?", new String[]{id});
  }

  public void deleteAllParticipants() {
    SQLiteDatabase db = getWritableDatabase();
    //db.execSQL("delete from " + TABLE_NAME);

  }

  public void deleteAllPairs() {
    SQLiteDatabase db = getWritableDatabase();
    db.execSQL("DELETE FROM " + PAIRS_TABLE);
  }


  public Cursor getAllPairs() {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("SELECT * FROM " + PAIRS_TABLE, null);
    return res;
  }

  public Cursor getPairByIds(Integer id1, Integer id2) {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("SELECT * FROM " + PAIRS_TABLE + " WHERE " + PARTICIPANT1_ID + " = " + id1 +
      " AND " + PARTICIPANT2_ID + " = " + id2, null);
    return res;
  }

  public Cursor getAllPairsById(Integer id1) {
    SQLiteDatabase db = getWritableDatabase();
    Cursor res = db.rawQuery("SELECT * FROM " + PAIRS_TABLE + " WHERE " + PARTICIPANT1_ID + " = " + id1 +
      " OR " + PARTICIPANT2_ID + " = " + id1 + " ORDER BY " + CONSECUTIVE_TIMES_SKIPPED + " DESC", null);
    return res;
  }


  public boolean addPair(String name1, String name2) {
    boolean isInserted = insertPair(name1, name2);
    if (isInserted == true)
      System.out.println("Pair Inserted");
    else
      System.out.println("Pair Not Inserted");

    return isInserted;
  }

  public Cursor initPairs() {
    //this doesn't work for some reason
/*
    SQLiteDatabase db = getWritableDatabase();
    String query = "UPDATE " + PAIRS_TABLE + " SET "
      + TIMES_PAIRED + " = 0, "
      + CONSECUTIVE_TIMES_SKIPPED + " = 0";

    Cursor res = db.rawQuery(query,null);

    return res;
*/
    SQLiteDatabase db = getWritableDatabase();
    ContentValues cv = new ContentValues();
    Cursor pairsCur = getAllPairs();
    while (pairsCur.moveToNext()) {
      Integer pairId = pairsCur.getInt(PAIR_FIELD_INDEXES.PAIR_ID_INDEX);
      Calendar cal = Calendar.getInstance();
      Long nowInMilliseconds = cal.getTimeInMillis();
      String nowInMilliseondsString = "" + nowInMilliseconds;
      cv.put(LAST_PAIRED_DATE, nowInMilliseondsString);
      cv.put(TIMES_PAIRED, 0);
      cv.put(CONSECUTIVE_TIMES_SKIPPED, 0);
      db.update(PairsStorage.PAIRS_TABLE, cv, PAIR_ID + " = ?", new String[]{"" + pairId});
    }

    return pairsCur;
  }

  public void updatePair(String name1, String name2, Integer timesPaired, Integer timesSkipped) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues cv = new ContentValues();
    Cursor participant1Cur = getParticipantByName(name1);
    if (participant1Cur.getCount() > 0) {
      //get first participant record
      if (participant1Cur.moveToNext()) {
        int participant1Id = participant1Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
        //get 2nd participant record
        Cursor participant2Cur = getParticipantByName(name2);
        if (participant2Cur.getCount() > 0) {
          if (participant2Cur.moveToNext()) {
            int participant2Id = participant2Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
            Cursor pairsCur = getPairByIds(participant1Id, participant2Id);
            if (pairsCur.getCount() > 0) {
              if (pairsCur.moveToNext()) {
                Integer pairId = pairsCur.getInt(PAIR_FIELD_INDEXES.PAIR_ID_INDEX);
                cv.put(TIMES_PAIRED, timesPaired);
                cv.put(CONSECUTIVE_TIMES_SKIPPED, timesSkipped);
                db.update(PairsStorage.PAIRS_TABLE, cv, PAIR_ID + " = ?", new String[]{"" + pairId});
              }
            }
            else {
              addPair(name1, name2);
            }
          }
        }
      }
    }
  }


  public void updatePair(String name1, String name2, boolean newlyPaired) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues cv = new ContentValues();
    Cursor participant1Cur = getParticipantByName(name1);
    if (participant1Cur.getCount() > 0) {
      //get first participant record
      if (participant1Cur.moveToNext()) {
        int participant1Id = participant1Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
        //get 2nd participant record
        Cursor participant2Cur = getParticipantByName(name2);
        if (participant2Cur.getCount() > 0) {
          if (participant2Cur.moveToNext()) {
            int participant2Id = participant2Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
            Cursor pairsCur = getPairByIds(participant1Id, participant2Id);
            if (pairsCur.getCount() > 0) {
              if (pairsCur.moveToNext()) {
                Integer pairId = pairsCur.getInt(PAIR_FIELD_INDEXES.PAIR_ID_INDEX);
                if (newlyPaired) {
                  Calendar cal = Calendar.getInstance();
                  Long nowInMilliseconds = cal.getTimeInMillis();
                  String nowInMilliseondsString = "" + nowInMilliseconds;
                  cv.put(LAST_PAIRED_DATE, nowInMilliseondsString);
                  Integer timesPaired = pairsCur.getInt(PAIR_FIELD_INDEXES.TIMES_PAIRED_INDEX);
                  cv.put(TIMES_PAIRED, ++timesPaired);
                  cv.put(CONSECUTIVE_TIMES_SKIPPED, 0);
                }
                else {
                  Integer consecutiveTimesSkipped = pairsCur.getInt(PAIR_FIELD_INDEXES.CONSECUTIVE_TIMES_SKIPPED_COUNT);
                  cv.put(CONSECUTIVE_TIMES_SKIPPED, ++consecutiveTimesSkipped);
                }
                db.update(PairsStorage.PAIRS_TABLE, cv, PAIR_ID + " = ?", new String[]{"" + pairId});
              }
            }
            else {
              addPair(name1, name2);
            }

          }
        }
      }
    }
  }

  public boolean insertPair(String name1, String name2) {
    SQLiteDatabase db = getWritableDatabase();
    ContentValues contentValues = new ContentValues();

    Cursor participant1Cur = getParticipantByName(name1);
    if (participant1Cur.getCount() > 0) {
      //get first participant record
      if (participant1Cur.moveToNext()) {
        int participant1Id = participant1Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);

        //get 2nd participant record
        Cursor participant2Cur = getParticipantByName(name2);
        if (participant2Cur.getCount() > 0) {
          if (participant2Cur.moveToNext()) {
            int participant2Id = participant2Cur.getInt(PARTICIPANT_FIELD_INDEXES.PARTICIPANT_ID_INDEX);
            contentValues.put(PARTICIPANT1_ID, participant1Id);
            contentValues.put(PARTICIPANT2_ID, participant2Id);
            Calendar cal = Calendar.getInstance();
            Long nowInMilliseconds = cal.getTimeInMillis();
            String nowInMilliseondsString = "" + nowInMilliseconds;
            contentValues.put(LAST_PAIRED_DATE, nowInMilliseondsString);
            contentValues.put(TIMES_PAIRED, 0);
            contentValues.put(CONSECUTIVE_TIMES_SKIPPED, 0);
            try {
              db.insertOrThrow(PAIRS_TABLE, null, contentValues);
              return true;
            }
            catch (Exception e) {
              String ex = e.getMessage();
              Log.d("exception", ex);
              return false;
            }
          }
        }
      }
    }
    return false;
  }

}


