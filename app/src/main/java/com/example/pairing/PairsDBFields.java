package com.example.pairing;

public class PairsDBFields {
  public final int PAIR_ID_INDEX = 0;
  public final int PARTICIPANT1_ID_INDEX = 1;
  public final int PARTICIPANT2_ID_INDEX = 2;
  public final int LAST_PAIRED_DATE_INDEX = 3;
  public final int TIMES_PAIRED_INDEX = 4;
  public final int CONSECUTIVE_TIMES_SKIPPED_COUNT = 5;
}
